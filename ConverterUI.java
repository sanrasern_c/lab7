import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.zip.DataFormatException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * To show the program in GUI.
 * @author Sanrasern Chaihetphon 5710547247
 */
public class ConverterUI extends JFrame {
	private JLabel equalSign;
	private JTextField input, result;
	private JComboBox<Unit> unit1, unit2;
	private JButton convert, clear;
	private UnitConverter uc;
	
	/**
	 * Contractor of the class.
	 * @param uc is class to convert unit.
	 */
	public ConverterUI(UnitConverter uc){
		this.uc = uc;
		initComponents();
		
	}
	/**
	 * To make GUI.
	 */
	private void initComponents(){
		
		super.setTitle("Distance Converter");
		JPanel pane = new JPanel();
		pane.setLayout(new FlowLayout());
		
		input = new JTextField(10);
		input.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					double amount = Double.parseDouble(input.getText());
					if(amount<0)
						throw new DataFormatException();
					Unit fromUnit = (Unit)unit1.getSelectedItem();
					Unit toUnit = (Unit)unit2.getSelectedItem();
					result.setText(uc.convert(amount, fromUnit, toUnit)+"");
				}catch(NumberFormatException ex){
					JOptionPane.showMessageDialog(ConverterUI.this, "please input number");
				}catch(DataFormatException ex2){
					JOptionPane.showMessageDialog(ConverterUI.this, "please input positive number");
				}
				
			}
		});
		
		unit1 = new JComboBox<Unit>(uc.getUnits());
		
		
		equalSign = new JLabel("=");
		
		result = new JTextField(10);
		result.setEditable(false);
		result.setBackground(Color.white);
		
		unit2 = new JComboBox<Unit>(uc.getUnits());
		
		convert = new JButton("Convert!");
		convert.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					double amount = Double.parseDouble(input.getText());
					if(amount<0)
						throw new DataFormatException();
					Unit fromUnit = (Unit)unit1.getSelectedItem();
					Unit toUnit = (Unit)unit2.getSelectedItem();
					result.setText(uc.convert(amount, fromUnit, toUnit)+"");
				}catch(NumberFormatException ex){
					JOptionPane.showMessageDialog(ConverterUI.this, "please input number");
				}catch(DataFormatException ex2){
					JOptionPane.showMessageDialog(ConverterUI.this, "please input positive number");
				}
				
			}
		});
		
		clear = new JButton("Clear");
		clear.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				input.setText("");
				result.setText("");
			}
		});
		
		
		
		pane.add(input);
		pane.add(unit1);
		pane.add(equalSign);
		pane.add(result);
		pane.add(unit2);
		pane.add(convert);
		pane.add(clear);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.add(pane);
		this.pack();
		this.setVisible(true);
	}
}
