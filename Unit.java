/**
 * Give the method to the class that call this class.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public interface Unit {
	double convertTo(double amount , Unit unit);
	double getValue();
	String toString();

}
