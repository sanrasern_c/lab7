/**
 * To change the value from one unit to another unit.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public class UnitConverter {
	/**
	 * Calculate the answer of convert unit.
	 * @param amount is amount that want to convert.
	 * @param fromUnit is unit of amount.
	 * @param toUnit is unit that want to convert to.
	 * @return amount in toUnit.
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit){
		return fromUnit.convertTo(amount, toUnit);
	}
	
	/**
	 * Get the all unit in Length in array. 
	 * @return all array of unit in Length.
	 */
	public Unit[] getUnits(){
		return Length.values();
	}
}
