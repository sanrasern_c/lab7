/**
 * This is keep the length unit.
 * @author Sanrasern Chaihetphon 5710547247
 *
 */
public enum Length implements Unit {
	METER("meter", 1.00 ),
	KILOMETER("km" , 1000.0),
	CENTIMETER("cm", 0.01),
	MILE("mile", 1609.344),
	FOOT("foot", 0.30480 ),
	WA("waaaa", 2.0);
	private double value;
	private String name;
	
	/**
	 * Contractor of Length
	 * @param name is name of unit.
	 * @param value is number to turn to base unit.
	 */
	private Length (String name, double value){
		this.name = name;
		this.value = value;
	}
	
	/**
	 * To change to base unit.
	 * @return double of base unit.
	 */
	@Override
	public double convertTo(double amount, Unit unit) {
		return  this.value*amount / unit.getValue();
	}
	
	/**
	 * Get the value.
	 * @return double value
	 */
	@Override
	public double getValue() {
		return this.value;
	}
	
	/**
	 * Get name of unit.
	 * @return String of unit.
	 */
	public String toString(){
		return  this.name;
	}
	
	
}
